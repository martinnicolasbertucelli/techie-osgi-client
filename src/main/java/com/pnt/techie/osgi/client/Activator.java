package com.pnt.techie.osgi.client;

import com.pnt.techie.osgi.service.definition.HelloService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

    private BundleContext ctx;

    public void start(BundleContext context) throws Exception {
        System.out.println("START ##########################");
        this.ctx = context;
        ServiceReference serviceReference = context.getServiceReference(HelloService.class);
        HelloService service = (HelloService) (context.getService(serviceReference));
        String out = service.saludar("PNT");
        System.out.println(out);
    }

    public void stop(BundleContext context) throws Exception {
        System.out.println("STOP #######################");
    }

}
